package com.example.zhbj.util;

import android.content.Context;

/**
 * 网络缓存的工具类
 */
public class CacheUtil {

    /**
     * 写缓存
     * @param context 上下文对象
     * @param url 标识
     * @param json 数据，要保存到本地
     */
    public static void setCache(Context context,String url, String json){
        PrefUtils.putString(context,url,json);
    }

    /**
     * 读缓存
     * @param context 上下文对象
     * @param url 标识
     */
    public static String getCache(Context context,String url){
        String data = PrefUtils.getString(context, url, null);
        return data;
    }
}
