package com.example.zhbj.util;

import android.content.Context;

/**
 *  长度单位转换的工具类
 */
public class DensityUtil {

    /**
     * 将dp转换成px
     * @param dp
     * @param context
     */
    public static int dpTopx(float dp, Context context){
        float density = context.getResources().getDisplayMetrics().density;
        int px = (int) (dp * density + 0.5f);
        return px;
    }

    /**
     * 将px转换成dp
     * @param px
     * @param context
     */
    public static float pxTodp(int px, Context context){
        float density = context.getResources().getDisplayMetrics().density;
        float dp = px / density;
        return dp;
    }
}
