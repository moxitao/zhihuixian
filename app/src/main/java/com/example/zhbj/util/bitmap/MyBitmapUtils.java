package com.example.zhbj.util.bitmap;

import android.graphics.Bitmap;
import android.widget.ImageView;

/**
 * 自定义三级缓存工具类
 */
public class MyBitmapUtils {

    /**
     * 内存缓存工具类对象
     */
    private MemoryCacheUtils mMemoryCacheUtils;

    /**
     * 本地缓存工具类对象
     */
    private LocalCacheUtils mLocalCacheUtils;

    /**
     * 网络缓存工具类对象
     */
    private NetCacheUtils mNetCacheUtils;

    public MyBitmapUtils() {
        mMemoryCacheUtils = new MemoryCacheUtils();
        mLocalCacheUtils = new LocalCacheUtils();
        mNetCacheUtils = new NetCacheUtils(mLocalCacheUtils,mMemoryCacheUtils);
    }

    /**
     * 加载图片进行展示
     * @param imageView ImageView组件
     * @param url uri地址
     */
    public void display(ImageView imageView,String url){
        // 一级缓存：内存缓存
        Bitmap bitmap = mMemoryCacheUtils.getMemoryCache(url);
        if (bitmap != null){
            imageView.setImageBitmap(bitmap);
            return;
        }

        // 二级缓存：本地缓存
        bitmap = mLocalCacheUtils.getLocalCache(url);
        if (bitmap != null){
            imageView.setImageBitmap(bitmap);
            // 写内存缓存
            mMemoryCacheUtils.setMemoryCache(url,bitmap);
            return;
        }

        // 三级缓存：网络缓存
        mNetCacheUtils.getBitmapFromNet(imageView,url);
    }
}
