package com.example.zhbj.util.bitmap;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * 网络缓存工具类
 */
public class NetCacheUtils {

    /**
     * ImaegView对象
     */
    private ImageView mImageView;

    /**
     * 图片url
     */
    private String url;

    /**
     * 内存缓存的工具类对象
     */
    private MemoryCacheUtils mMemoryCacheUtils;

    /**
     * 本地缓存的工具类对象
     */
    private LocalCacheUtils mLocalCacheUtils;

    public NetCacheUtils(LocalCacheUtils localCacheUtils, MemoryCacheUtils memoryCacheUtils) {
        mLocalCacheUtils = localCacheUtils;
        mMemoryCacheUtils = memoryCacheUtils;
    }

    /**
     * 异步下载图片
     * @param imageView ImageView组件
     * @param url uri地址
     */
    public void getBitmapFromNet(ImageView imageView, String url) {
        // 调用AsyncTask进行异步下载
        new BitmapTask().execute(imageView,url);
    }

    class BitmapTask extends AsyncTask<Object,Void,Bitmap>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Bitmap doInBackground(Object... params) {
            mImageView = (ImageView) params[0];
            url = (String) params[1];
            // 给当前ImageView打上标签
            mImageView.setTag(url);
            // 使用url下载图片
            Bitmap bitmap = download(url);
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            // 将下载好的图片设置给ImaegView
            /*
             注意：由于ListView的重用机制，导致某个item有可能展示它所重用的那个item的图片，导致图片错乱（即数据不同步）
             解决方案：确保当前设置的图片和当前显示的ImageView完全匹配
             */
            if (bitmap != null){
                String tag = (String) mImageView.getTag(); // 获取和当前ImageView绑定的url
                if (url.equals(tag)){ // 判断当前下载的图片url是否和ImageView中的url一致，如果一致，说明图片正确
                    mImageView.setImageBitmap(bitmap);

                    // 写本地缓存
                    mLocalCacheUtils.setLocalCache(url,bitmap);

                    // 写内存缓存
                    mMemoryCacheUtils.setMemoryCache(url,bitmap);
                }
            }
        }
    }

    /**
     * 使用url下载图片
     * @param url url地址
     * @return
     */
    public Bitmap download(String url) {
        // 创建连接对象
        HttpURLConnection conn = null;
        try {
            conn = (HttpURLConnection) new URL(url).openConnection();
            conn.setRequestMethod("GET");
            conn.setConnectTimeout(6000);
            conn.setReadTimeout(6000);
            conn.connect();
            int responseCode = conn.getResponseCode();
            if (responseCode == 200){
                InputStream in = conn.getInputStream();
                // 使用输入流生成一个Bitmap对象
                Bitmap bitmap = BitmapFactory.decodeStream(in);
                return bitmap;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (conn != null){
                conn.disconnect();
            }
        }
        return null;
    }
}
