package com.example.zhbj.util.bitmap;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v4.util.LruCache;

import java.lang.ref.SoftReference;
import java.util.HashMap;

/**
 * 内存缓存工具类
 */
public class MemoryCacheUtils {

    /**
     * 存储内存空间的map对象
     */
    private HashMap<String, SoftReference<Bitmap>> mHashMap = new HashMap<>();

    /**
     * LruCache对象实例
     */
    private LruCache<String,Bitmap> mLrucache;

    public MemoryCacheUtils() {
        long maxMemory = Runtime.getRuntime().maxMemory(); // 获取虚拟机分配的最大内存，默认为16mb
        // 需要传入一个内存缓存上限
        mLrucache = new LruCache<String,Bitmap>((int) (maxMemory / 8)){

            /**
             * 返回单个对象占用内存的大小
             * @param key
             * @param value
             * @return
             */
            @Override
            protected int sizeOf(@NonNull String key, @NonNull Bitmap value) {
                // 计算图片占用的内存大小
                int byteCount = value.getByteCount();
                return byteCount;
            }
        };
    }

    /**
     * 写缓存
     * @param url url地址
     * @param bitmap bitmap对象
     */
    public void setMemoryCache(String url,Bitmap bitmap){
        /*
        SoftReference<Bitmap> soft = new SoftReference<Bitmap>(bitmap); // 用软引用包装btmap
        mHashMap.put(url,soft);
         */
        mLrucache.put(url,bitmap);
    }

    /**
     * 读缓存
     * @param url url地址
     * @return
     */
    public Bitmap getMemoryCache(String url){
        /*
        SoftReference<Bitmap> soft = mHashMap.get(url);
        if (soft != null){
            Bitmap bitmap = soft.get(); // 从软引用取出当前对象
            return bitmap;
        }
        return null;
         */
        return mLrucache.get(url);
    }
}
