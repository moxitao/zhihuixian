package com.example.zhbj.util.bitmap;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;

import com.example.zhbj.util.MD5Encoder;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

/**
 * 本地缓存工具类
 */
public class LocalCacheUtils {

    /**
     * 缓存文件夹的路径
     */
    private String PATH = Environment.getExternalStorageDirectory().getAbsolutePath() + "/zhbj_cache/";;

    /**
     * 写缓存
     * @param url url地址
     * @param bitmap 图片
     */
    public void setLocalCache(String url,Bitmap bitmap){
        // 将图片保存在本地文件
        File dir = new File(PATH);
        if (!dir.exists() || !dir.isDirectory()){
            dir.mkdirs(); // 创建文件夹
        }
        try {
            File cacheFile = new File(dir, MD5Encoder.encode(url)); // 创建本地的文件，以url的md5命名
            // 将图片压缩保存在本地；参数1：图片格式，参数2：压缩比（0-100,100表示不压缩），参数3：输出流
            bitmap.compress(Bitmap.CompressFormat.JPEG,100,new FileOutputStream(cacheFile));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * 读缓存
     * @param url url地址
     */
    public Bitmap getLocalCache(String url){
        try {
            File cacheFile = new File(url,MD5Encoder.encode(url)); // 创建本地的文件，以url的md5命名
            if (cacheFile.exists()){
                // 缓存存在
                Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(cacheFile));
                return bitmap;
            }
        }catch (Exception e){
        }
        return null;
    }
}
