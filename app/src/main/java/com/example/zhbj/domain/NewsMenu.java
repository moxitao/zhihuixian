package com.example.zhbj.domain;

import java.util.ArrayList;

/**
 * 新闻分类封装的实体类
 */
public class NewsMenu {

    public int retcode;

    public ArrayList<NewsMenuData> data;

    public ArrayList<String> extend;

    /**
     * 四个分类菜单的信息
     */
    public class NewsMenuData{

        public String id;

        public String title;

        public int type;

        public ArrayList<NewsTabData> children;
    }

    /**
     * 十二个页签的对象封装
     */
    public class NewsTabData{

        public String id;

        public String title;

        public int type;

        public String url;

    }
}
