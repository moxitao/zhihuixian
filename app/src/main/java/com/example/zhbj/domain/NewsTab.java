package com.example.zhbj.domain;

import java.util.ArrayList;

/**
 * 页签网络数据
 */
public class NewsTab {

    public NewsTabData data;

    public class NewsTabData{
        public String more;
        public ArrayList<TopNews> topnews;
        public ArrayList<News> news;
    }

    public class TopNews{
        public String id;
        public String pubdate;
        public String title;
        public String topimage;
        public String url;
    }

    public class News{
        public String id;
        public String pubdate;
        public String title;
        public String listimage;
        public String url;
    }
}
