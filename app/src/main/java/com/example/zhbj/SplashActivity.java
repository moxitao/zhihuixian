package com.example.zhbj;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.widget.RelativeLayout;

import com.example.zhbj.util.PrefUtils;

/**
 * 闪屏页面
 */
public class SplashActivity extends Activity {

    /**
     * 闪屏页面的根布局
     */
    private RelativeLayout rlRoot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        rlRoot = (RelativeLayout) findViewById(R.id.rl_root);

        // 旋转
        RotateAnimation rotateAnimation = new RotateAnimation(0,360,Animation.RELATIVE_TO_SELF,0.5f,Animation.RELATIVE_TO_SELF,0.5f); // 基于自身中心点旋转360度
        rotateAnimation.setDuration(1000); // 动画时间
        rotateAnimation.setFillAfter(true); // 保持住动画结束的状态

        // 缩放
        ScaleAnimation scaleAnimation = new ScaleAnimation(0,1,0,1,Animation.RELATIVE_TO_SELF,0.5f,Animation.RELATIVE_TO_SELF,0.5f);
        scaleAnimation.setDuration(1000);
        scaleAnimation.setFillAfter(true);

        // 渐变
        AlphaAnimation alphaAnimation = new AlphaAnimation(0,1);
        alphaAnimation.setDuration(2000);
        alphaAnimation.setFillAfter(true);

        // 动画集合
        AnimationSet animationSet = new AnimationSet(false);
        animationSet.addAnimation(rotateAnimation);
        animationSet.addAnimation(scaleAnimation);
        animationSet.addAnimation(alphaAnimation);

        // 启动动画
        rlRoot.startAnimation(animationSet);

        // 设置监听
        animationSet.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                // 判断有没有展示过引导页
                boolean isGuideShow = PrefUtils.getBoolean(getApplicationContext(), "is_guide_show", false);
                if (!isGuideShow){
                    // 动画结束后，跳转到引导页
                    startActivity(new Intent(getApplicationContext(),GuideActivity.class));
                }else {
                    // 跳到主页面
                    startActivity(new Intent(getApplicationContext(),MainActivity.class));
                }
                finish();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }
}
