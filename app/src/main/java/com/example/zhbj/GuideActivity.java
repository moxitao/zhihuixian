package com.example.zhbj;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.example.zhbj.util.DensityUtil;
import com.example.zhbj.util.PrefUtils;

import java.util.ArrayList;

/**
 * 引导页面
 */
public class GuideActivity extends Activity {

    /**
     * ViewPaper的实例
     */
    private ViewPager mViewPaper;

    /**
     * 灰点指示器的实例
     */
    private LinearLayout llContainer;

    /**
     * 引导页面的图片id数组
     */
    private int[] mImageIds = new int[]{R.drawable.guide_1,R.drawable.guide_2,R.drawable.guide_3};

    /**
     * 存储引导页面图片的集合
     */
    private ArrayList<ImageView> mImageViews;

    /**
     * 圆点移动的距离
     */
    private int mPointDis;

    /**
     * 小红点的实例对象
     */
    private ImageView ivRedPoint;

    /**
     * “开始体验”按钮的实例对象
     */
    private Button btnstart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); // 从代码中去掉标题栏，必须在setContentView之前调用
        setContentView(R.layout.activity_guide);
        initViews();
        initData();
    }

    /**
     * 初始化布局
     */
    private void initViews() {
        mViewPaper = (ViewPager) findViewById(R.id.vp_guide);
        llContainer = (LinearLayout) findViewById(R.id.ll_container);
        ivRedPoint = (ImageView) findViewById(R.id.iv_red_point);
        btnstart = (Button) findViewById(R.id.btn_start);
    }

    /**
     * 初始化数据
     */
    private void initData(){
        // 初始化3张图片的ImageView
        mImageViews = new ArrayList<>();
        for (int i = 0; i < mImageIds.length; i++) {
            ImageView view = new ImageView(this);
            view.setBackgroundResource(mImageIds[i]);
            mImageViews.add(view);

            // 初始化小灰点
            ImageView point = new ImageView(this);
            point.setImageResource(R.drawable.shape_point_normal);

            // 初始化布局参数
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT);
            if (i > 0){
                //params.leftMargin = 10; // 从第二个点开始设置左边距，单位是px（像素）
                params.leftMargin = DensityUtil.dpTopx(10,this);
            }
            point.setLayoutParams(params); // 设置布局参数
            llContainer.addView(point);

        }
        mViewPaper.setAdapter(new GuideAdapter());

        // 监听ViewPager滑动事件，更新小红点位置
        mViewPaper.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            // 监听滑动事件
            @Override
            public void onPageScrolled(int i, float v, int i1) {
                
                // 通过修改小红点的左边距，来更新小红点的位置
                int leftMargin = (int) (mPointDis * v + mPointDis * i + 0.5f); // 要将当前的位置信息产生的距离加进来

                // 获取小红点的布局参数
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) ivRedPoint.getLayoutParams();
                params.leftMargin = leftMargin;
                ivRedPoint.setLayoutParams(params);
            }

            @Override
            public void onPageSelected(int i) {
                if (i == mImageIds.length - 1){
                    // 最后一个页面显示“开始体验”按钮
                    btnstart.setVisibility(View.VISIBLE);
                }
                else {
                    btnstart.setVisibility(View.GONE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        // 计算圆点移动距离 = 第二个圆点的左边距 - 第一个圆点的左边距
        // measure -> layout -> draw，必须onCreate执行结束之后，才会开始绘制布局，走这三个方法
        // mPointDis = llContainer.getChildAt(1).getLeft() - llContainer.getChildAt(0).getLeft();

        // 监听layout执行结束的事件，一旦结束之后，再去获取当前的left的位置
        // 视图树
        ivRedPoint.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            // 一旦视图树的layout方法调用完成，就会回调此方法
            @Override
            public void onGlobalLayout() {
                // 布局位置已经确定，可以拿到位置信息了
                mPointDis = llContainer.getChildAt(1).getLeft() - llContainer.getChildAt(0).getLeft();

                // 移除观察者
                // ivRedPoint.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                ivRedPoint.getViewTreeObserver().removeGlobalOnLayoutListener(this);
            }
        });

        // 开始按钮的点击事件
        btnstart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 在sp中记录访问过引导页的状态
                PrefUtils.putBoolean(getApplicationContext(),"is_guide_show",true);

                startActivity(new Intent(getApplicationContext(),MainActivity.class));
                finish();
            }
        });
    }

    /**
     * ViewPager的适配器内部类
     */
    class GuideAdapter extends PagerAdapter{

        @Override
        public int getCount() {
            return mImageIds.length;
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
            return view == o;
        }

        /**
         * 初始化布局
         * @param container
         * @param position
         * @return
         */
        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            ImageView view = mImageViews.get(position);
            container.addView(view);
            return view;
        }

        /**
         * 销毁布局
         * @param container
         * @param position
         * @param object
         */
        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            container.removeView((View) object);
        }
    }
}
