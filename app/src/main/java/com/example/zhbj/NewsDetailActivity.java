package com.example.zhbj;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

import cn.sharesdk.onekeyshare.OnekeyShare;

/**
 * 新闻详情页
 */
public class NewsDetailActivity extends Activity implements View.OnClickListener {

    /**
     * 返回按钮
     */
    @ViewInject(R.id.btn_back)
    private ImageButton btnBack;

    /**
     * 菜单按钮
     */
    @ViewInject(R.id.btn_menu)
    private ImageButton btnMenu;

    /**
     * 包含两个功能按钮的线性布局
     */
    @ViewInject(R.id.ll_control)
    private LinearLayout llControl;

    /**
     * 字体大小按钮
     */
    @ViewInject(R.id.btn_textsize)
    private ImageButton btnTextSize;

    /**
     * 分享按钮
     */
    @ViewInject(R.id.btn_share)
    private ImageButton btnShare;

    /**
     * WebView的实例
     */
    @ViewInject(R.id.webview)
    private WebView mWebView;

    /**
     * 进度条实例
     */
    @ViewInject(R.id.pb_loading3)
    private ProgressBar pbLoading;

    /**
     * 选中的字体大小标识符
     */
    private int mTempWhich;

    /**
     * 当前选中的字体大小标识符
     */
    private int mCurrentWhich = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_news_detail);
        ViewUtils.inject(this);
        initViews();

        // 获取WebView的设置对象
        WebSettings settings = mWebView.getSettings();
        settings.setJavaScriptEnabled(true); // 启用js功能
        settings.setBuiltInZoomControls(true); // 启用显示放大/缩小的按钮，不支持已经适配好移动端的页面
        settings.setUseWideViewPort(true); // 启用双击缩放
        // 给WebView设置一个监听
        mWebView.setWebViewClient(new WebViewClient(){
            /**
             * 页面开始加载
             * @param view
             * @param url
             * @param favicon
             */
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                pbLoading.setVisibility(View.VISIBLE);
            }

            /**
             * 跳转链接
             * @param view
             * @param url
             * @return
             */
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                // 强制所有跳转链接都在WebView执行
                mWebView.loadUrl(url);
                return true;
            }

            /**
             * 加载结束
             * @param view
             * @param url
             */
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                pbLoading.setVisibility(View.GONE);
            }
        });
        mWebView.setWebChromeClient(new WebChromeClient(){

            /**
             * 获取网页标题
             * @param view
             * @param title
             */
            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);
            }

            /**
             * 进度发生变化
             * @param view
             * @param newProgress
             */
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
            }
        });

        String url = getIntent().getStringExtra("url");

        // 开始加载网页
        mWebView.loadUrl(url);
    }

    /**
     * 初始化布局
     */
    private void initViews() {
        btnBack.setVisibility(View.VISIBLE);
        btnMenu.setVisibility(View.GONE);
        llControl.setVisibility(View.VISIBLE);
        btnBack.setOnClickListener(this);
        btnMenu.setOnClickListener(this);
        btnTextSize.setOnClickListener(this);
        btnShare.setOnClickListener(this);
    }

    /**
     * 拦截物理返回键
     */
    @Override
    public void onBackPressed() {
        if (mWebView.canGoBack()){ // 判断是否可以返回
            mWebView.goBack(); // 返回上一个网页
            // mWebView.goForward(); 跳到下一个网页（前提是有一个历史记录）
            // mWebView.canGoForward(); 判断是否可以跳转到下一个网页
        }
        else {
            finish();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_back:
                finish();
                break;
            case R.id.btn_textsize:
                showChooseDialog();
                break;
            case R.id.btn_share:
                showShare();
                break;
            default:
                break;
        }
    }

    /**
     * 显示选择字体的弹窗
     */
    private void showChooseDialog() {
        System.out.println("调用了方法");
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("字体设置");
        String[] items = new String[]{"超大号字体","大号字体","正常字体","小号字体","超小号字体"};
        // 显示单选框，参数1：单选字符串数组;参数2：当前默认选中的位置;参数3：选中的监听器
        builder.setSingleChoiceItems(items, mCurrentWhich, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mTempWhich = which;
            }
        });

        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                WebSettings settings = mWebView.getSettings();
                switch (mTempWhich){
                    case 0:
                        // 超大号字体
                        settings.setTextSize(WebSettings.TextSize.LARGEST);
                        break;
                    case 1:
                        // 大号字体
                        settings.setTextSize(WebSettings.TextSize.LARGER);
                        break;
                    case 2:
                        // 正常字体
                        settings.setTextSize(WebSettings.TextSize.NORMAL);
                        break;
                    case 3:
                        // 小号字体
                        settings.setTextSize(WebSettings.TextSize.SMALLER);
                        break;
                    case 4:
                        // 超小号字体
                        settings.setTextSize(WebSettings.TextSize.SMALLEST);
                        break;
                    default:
                        break;
                }
                mCurrentWhich = mTempWhich;
            }
        });

        builder.setNegativeButton("取消",null);

        builder.show();
    }

    /**
     * ShareSDK的分享方法
     */
    private void showShare() {
        OnekeyShare oks = new OnekeyShare();
        // title标题，微信、QQ和QQ空间等平台使用
        oks.setTitle("分享");
        // titleUrl QQ和QQ空间跳转链接
        oks.setTitleUrl("http://sharesdk.cn");
        // text是分享文本，所有平台都需要这个字段
        oks.setText("我是分享文本");
        // imagePath是图片的本地路径，确保SDcard下面存在此张图片
        oks.setImagePath("/sdcard/test.jpg");
        // url在微信、Facebook等平台中使用
        oks.setUrl("http://sharesdk.cn");
        // 启动分享GUI
        oks.show(this);
    }
}
