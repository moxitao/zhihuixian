package com.example.zhbj.global;

public class GlobalConstans {

    /**
     * 服务器根域名，地址
     */
    public static final String SERVER_URL = "http://10.0.2.2:8080/zhbj";

    /**
     * 分类接口地址
     */
    public static final String CATEGORY_URL =  SERVER_URL + "/categories.json";

    /**
     * 组图接口地址
     */
    public static final String PHOTOS_URL =  SERVER_URL + "/photos/photos_1.json";
}
