package com.example.zhbj.base;

import android.app.Activity;
import android.view.View;

/**
 * 菜单详情页基类，在新闻专题组图互动
 */
public abstract class BaseMenuDetailPaper {

    /**
     * Activity对象
     */
    public Activity mActivity;

    /**
     * 菜单详情页的根布局
     */
    public View mRootView;

    public BaseMenuDetailPaper(Activity activity) {
        mActivity = activity;
        mRootView = initViews();
    }

    /**
     * 初始化布局，必须由子类实现
     */
    public abstract View initViews();
    /**
     * 初始化页面
     */
    public void initData(){

    }
}
