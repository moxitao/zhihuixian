package com.example.zhbj.base.impl;

import android.app.Activity;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import com.example.zhbj.MainActivity;
import com.example.zhbj.base.BaseMenuDetailPaper;
import com.example.zhbj.base.BasePaper;
import com.example.zhbj.base.impl.menudetail.InteractMenuDetailPaper;
import com.example.zhbj.base.impl.menudetail.NewsMenuDetailPaper;
import com.example.zhbj.base.impl.menudetail.PhotosMenuDetailPaper;
import com.example.zhbj.base.impl.menudetail.TopicMenuDetailPaper;
import com.example.zhbj.domain.NewsMenu;
import com.example.zhbj.fragment.LeftMenuFragment;
import com.example.zhbj.global.GlobalConstans;
import com.example.zhbj.util.CacheUtil;
import com.google.gson.Gson;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest;

import java.util.ArrayList;

/**
 * 新闻中心
 */
public class NewsCenterPaper extends BasePaper {

    /**
     * 存放四个菜单详情页对象的集合
     */
    private ArrayList<BaseMenuDetailPaper> mPapers;

    /**
     * 从服务器上获取到的JSON数据
     */
    private NewsMenu newsMenu;

    public NewsCenterPaper(Activity activity) {
        super(activity);
    }

    @Override
    public void initData() {
        // 给空的帧布局动态添加布局对象
        /*
        TextView view = new TextView(mActivity);
        view.setTextSize(22);
        view.setTextColor(Color.RED);
        view.setGravity(Gravity.CENTER); // 居中显示
        view.setText("新闻中心");
        flContainer.addView(view); // 给帧布局添加对象
        */

        // 设置标题
        tvTitle.setText("新闻");

        String cache = CacheUtil.getCache(mActivity, GlobalConstans.CATEGORY_URL);
        if (!TextUtils.isEmpty(cache)) {
            // 有缓存
            processData(cache);
        }
        /*
        }else {
            // 无缓存，从服务器获取数据
            getDataFromServer();
        }
        */
        // 继续请求服务器数据，保证缓存更新
        getDataFromServer();

    }

    /**
     * 从服务器获取数据
     */
    private void getDataFromServer() {

        // Xutils
        HttpUtils utils = new HttpUtils();
        // 原生模拟器：10.0.2.2；Genymotion:10.0.3.2
        utils.send(HttpRequest.HttpMethod.GET, GlobalConstans.CATEGORY_URL,
                new RequestCallBack<String>() {

                    @Override
                    public void onSuccess(ResponseInfo<String> responseInfo) {
                        String result = responseInfo.result;
                        System.out.println("服务器分类数据：" + result);
                        processData(result);

                        // 写缓存
                        CacheUtil.setCache(mActivity,GlobalConstans.CATEGORY_URL,result);
                    }

                    @Override
                    public void onFailure(HttpException e, String s) {
                    }
                });
    }

    /**
     * 解析从服务器获取的JSON数据
     */
    private void processData(String json) {
        Gson gson = new Gson();
        // 通过json和对象类，来生成一个对象
        newsMenu = gson.fromJson(json, NewsMenu.class);

        // 找到侧边栏对象
        MainActivity mainUI = (MainActivity) mActivity;
        LeftMenuFragment fragment = mainUI.getLeftMenuFragment();
        fragment.setMenuData(newsMenu.data);

        // 网络请求成功之后，初始化四个菜单详情页
        mPapers = new ArrayList<>();
        mPapers.add(new NewsMenuDetailPaper(mActivity,newsMenu.data.get(0).children)); // 通过构造方法传递数据
        mPapers.add(new TopicMenuDetailPaper(mActivity));
        mPapers.add(new PhotosMenuDetailPaper(mActivity,btnDisplay));
        mPapers.add(new InteractMenuDetailPaper(mActivity));

        // 设置新闻菜单详情页为默认页面
        setMenuDetailPager(0);
    }

    /**
     * 设置新闻中心的详情页
     * @param position
     */
    public void setMenuDetailPager(int position){
        BaseMenuDetailPaper paper = mPapers.get(position);

        // 判断是否是组图，如果是，显示切换按钮，否则隐藏
        if (paper instanceof PhotosMenuDetailPaper){
            btnDisplay.setVisibility(View.VISIBLE);
        }else {
            btnDisplay.setVisibility(View.GONE);
        }

        // 清除之前帧布局显示的内容
        flContainer.removeAllViews();

        // 修改当前帧布局显示的内容
        flContainer.addView(paper.mRootView);

        // 初始化当前页面的数据
        paper.initData();

        // 修改标题栏
        tvTitle.setText(newsMenu.data.get(position).title);
    }
}
