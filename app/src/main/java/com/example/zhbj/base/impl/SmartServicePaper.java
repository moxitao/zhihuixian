package com.example.zhbj.base.impl;

import android.app.Activity;
import android.graphics.Color;
import android.view.Gravity;
import android.widget.TextView;

import com.example.zhbj.base.BasePaper;

/**
 * 智慧服务
 */
public class SmartServicePaper extends BasePaper {

    public SmartServicePaper(Activity activity) {
        super(activity);
    }

    @Override
    public void initData() {
        // 给空的帧布局动态添加布局对象
        TextView view = new TextView(mActivity);
        view.setTextSize(22);
        view.setTextColor(Color.RED);
        view.setGravity(Gravity.CENTER); // 居中显示
        view.setText("智慧服务");
        flContainer.addView(view); // 给帧布局添加对象

        // 设置标题
        tvTitle.setText("生活");
    }
}
