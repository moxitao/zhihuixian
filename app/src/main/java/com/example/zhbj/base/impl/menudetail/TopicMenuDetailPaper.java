package com.example.zhbj.base.impl.menudetail;

import android.app.Activity;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import com.example.zhbj.base.BaseMenuDetailPaper;

/**
 * 菜单详情页 - 专题
 */
public class TopicMenuDetailPaper extends BaseMenuDetailPaper {

    public TopicMenuDetailPaper(Activity activity) {
        super(activity);
    }

    @Override
    public View initViews() {
        // 给空的帧布局动态添加布局对象
        TextView view = new TextView(mActivity);
        view.setTextSize(22);
        view.setTextColor(Color.RED);
        view.setGravity(Gravity.CENTER); // 居中显示
        view.setText("菜单详情页-专题");
        return view;
    }
}
