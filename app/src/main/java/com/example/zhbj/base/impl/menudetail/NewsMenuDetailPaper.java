package com.example.zhbj.base.impl.menudetail;

import android.app.Activity;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.zhbj.MainActivity;
import com.example.zhbj.R;
import com.example.zhbj.base.BaseMenuDetailPaper;
import com.example.zhbj.base.impl.TabDetailPaper;
import com.example.zhbj.domain.NewsMenu;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.viewpagerindicator.TabPageIndicator;

import java.util.ArrayList;

/**
 * 菜单详情页 - 新闻
 */
public class NewsMenuDetailPaper extends BaseMenuDetailPaper implements ViewPager.OnPageChangeListener {

    /**
     * ViewPager对象
     */
    @ViewInject(R.id.vp_news_menu_detail)
    private ViewPager mViewPager;

    /**
     * indicator指示器
     */
    @ViewInject(R.id.indicator)
    private TabPageIndicator mIndicator;

    /**
     * 存储标题
     */
    private ArrayList<NewsMenu.NewsTabData> children;

    /**
     * 存储详情页
     */
    private ArrayList<TabDetailPaper> mPaper;

    public NewsMenuDetailPaper(Activity activity, ArrayList<NewsMenu.NewsTabData> children) {
        super(activity);
        this.children = children;
    }

    @Override
    public View initViews() {
        // 给空的帧布局动态添加布局对象
        /*
        TextView view = new TextView(mActivity);
        view.setTextSize(22);
        view.setTextColor(Color.RED);
        view.setGravity(Gravity.CENTER); // 居中显示
        view.setText("菜单详情页-新闻");
        */
        View view = View.inflate(mActivity, R.layout.paper_news_menu_detail, null);
        ViewUtils.inject(this,view);
        return view;
    }

    @Override
    public void initData() {
        // 初始化12个页签对象，具体数量以服务器为准
        mPaper = new ArrayList<>();
        for (int i = 0; i < children.size(); i++) {
            TabDetailPaper paper = new TabDetailPaper(mActivity,children.get(i));
            mPaper.add(paper);
        }
        mViewPager.setAdapter(new NewsMenuDetailAdapter());
        mIndicator.setViewPager(mViewPager); // 将ViewPager和ViewPageIndicator绑定在一起，注意需要在setAdapter结束之后再调

        // 设置页面的触摸监听
        // mViewPager.setOnPageChangeListener(this);
        mIndicator.setOnPageChangeListener(this); // 当ViewPager和Indicator绑定时，事件需要设置给mIndicator
    }

    /**
     * 通过Xutils来添加按钮的点击事件
     * 注意：在xml中，配置组件的onClick属性只适用于Activity
     * @param view
     */
    @OnClick(R.id.btn_next)
    public void nextPage(View view){
        // 跳转到下一个页面
        int currentPos = mViewPager.getCurrentItem();
        //mIndicator.setCurrentItem(++currentPos); 会导致显示bug
        mViewPager.setCurrentItem(++currentPos);
    }

    @Override
    public void onPageScrolled(int i, float v, int i1) {

    }

    @Override
    public void onPageSelected(int i) {
        if (i == 0){
            // 打开侧边栏
            setSlidingMenuEnable(true);
        }else {
            // 禁用侧边栏
            setSlidingMenuEnable(false);
        }
    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }

    /**
     * 开启/禁用侧边栏
     */
    private void setSlidingMenuEnable(boolean enable){
        // 获取MainActivity对象
        MainActivity mainUI = (MainActivity) mActivity;

        // 获取SlidingMenu对象
        SlidingMenu slidingMenu = mainUI.getSlidingMenu();
        if (enable){
            slidingMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
        }else {
            slidingMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);
        }

    }

    class NewsMenuDetailAdapter extends PagerAdapter{

        @Override
        public int getCount() {
            return mPaper.size();
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
            return view == o;
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            TabDetailPaper paper = mPaper.get(position);
            paper.initData(); // 初始化数据
            container.addView(paper.mRootView);
            return paper.mRootView;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            container.removeView((View) object);
        }

        /**
         * 返回指示器Indicator的标题
         * @param position
         * @return
         */
        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return children.get(position).title;
        }
    }


}
