package com.example.zhbj.base;

import android.app.Activity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.zhbj.MainActivity;
import com.example.zhbj.R;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;

/**
 * 5个标签页的基类
 *
 * 共性：子类都有标题栏，所以可以直接在父类中加载布局页面
 */
public class BasePaper {

    /**
     * Activity对象
     */
    public Activity mActivity;

    /**
     * 标题的对象
     */
    public TextView tvTitle;

    /**
     * 标题按钮的对象
     */
    public ImageButton btnMenu;

    /**
     * 当前页面的根布局
     */
    public View mRootView;

    /**
     * 切换布局的对象
     */
    public ImageButton btnDisplay;

    /**
     * 内容的对象
     */
    public FrameLayout flContainer;

    public BasePaper(Activity activity) {
        mActivity = activity;
        // 在页面对象创建时，就初始化了布局
        mRootView = initView();
    }

    /**
     * 初始化布局
     */
    public View initView(){
        View view = View.inflate(mActivity, R.layout.base_paper, null);
        tvTitle = (TextView) view.findViewById(R.id.tv_title);
        btnMenu = (ImageButton) view.findViewById(R.id.btn_menu);
        flContainer = (FrameLayout) view.findViewById(R.id.fl_container);
        btnDisplay = (ImageButton) view.findViewById(R.id.btn_display);
        // 点击菜单按钮，控制侧边栏开关
        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggle();
            }
        });
        return view;
    }

    /**
     * 控制侧边栏的开关
     */
    private void toggle() {
        MainActivity mainUI = (MainActivity) mActivity;
        SlidingMenu slidingMenu = mainUI.getSlidingMenu();
        slidingMenu.toggle(); // 如果当前为开，则关；反之亦然
    }

    /**
     * 初始化数据
     */
    public void initData(){

    }
}
