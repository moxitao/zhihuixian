package com.example.zhbj.base.impl.menudetail;

import android.app.Activity;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.zhbj.R;
import com.example.zhbj.base.BaseMenuDetailPaper;
import com.example.zhbj.domain.PhotosBean;
import com.example.zhbj.global.GlobalConstans;
import com.example.zhbj.util.CacheUtil;
import com.google.gson.Gson;
import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest;
import com.lidroid.xutils.view.annotation.ViewInject;

import java.util.ArrayList;

/**
 * 菜单详情页 - 组图
 */
public class PhotosMenuDetailPaper extends BaseMenuDetailPaper implements View.OnClickListener {

    /**
     * ListView实例对象
     */
    @ViewInject(R.id.lv_list2)
    private ListView lvList;

    /**
     * GridView实例对象
     */
    @ViewInject(R.id.gv_list)
    private GridView gvList;

    /**
     * 存储照片新闻的集合
     */
    private ArrayList<PhotosBean.PhotoNews> mPhotoList;

    /**
     * 判断是否是ListView
     */
    private boolean isListView = true;

    private ImageButton btnDisplay;

    public PhotosMenuDetailPaper(Activity mActivity, ImageButton btnDisplay) {
        super(mActivity);
        this.btnDisplay = btnDisplay;
        btnDisplay.setOnClickListener(this); // 设置切换按钮的监听
    }


    @Override
    public View initViews() {
        // 给空的帧布局动态添加布局对象
        /*
        TextView view = new TextView(mActivity);
        view.setTextSize(22);
        view.setTextColor(Color.RED);
        view.setGravity(Gravity.CENTER); // 居中显示
        view.setText("菜单详情页-组图");
         */
        View view = View.inflate(mActivity, R.layout.paper_photos_menu_detail,null);
        ViewUtils.inject(this,view);
        return view;
    }

    /**
     * 初始化数据
     */
    @Override
    public void initData() {
        String cache = CacheUtil.getCache(mActivity, GlobalConstans.PHOTOS_URL);
        if (!TextUtils.isEmpty(cache)){
            processData(cache);
        }
        getDataFromServer();
    }

    /**
     * 从服务器获取数据
     */
    private void getDataFromServer() {
        HttpUtils utils = new HttpUtils();
        utils.send(HttpRequest.HttpMethod.GET, GlobalConstans.PHOTOS_URL, new RequestCallBack<String>() {
            @Override
            public void onSuccess(ResponseInfo<String> responseInfo) {
                String result = responseInfo.result;
                processData(result);
                CacheUtil.setCache(mActivity,GlobalConstans.PHOTOS_URL,result);
            }

            @Override
            public void onFailure(HttpException e, String s) {

            }
        });
    }

    /**
     * 加载数据
     * @param result
     */
    private void processData(String result){
        Gson gson = new Gson();
        PhotosBean photosBean = gson.fromJson(result, PhotosBean.class);
        mPhotoList = photosBean.data.news;
        // 给ListView设置数据
        lvList.setAdapter(new PhotosAdapter());

        // 给GridView设置数据
        gvList.setAdapter(new PhotosAdapter());
    }

    @Override
    public void onClick(View v) {
        if (isListView){
            // 显示GridView
            lvList.setVisibility(View.GONE);
            gvList.setVisibility(View.VISIBLE);
            btnDisplay.setImageResource(R.drawable.icon_pic_list_type);
            isListView = false;
        }else {
            // 显示ListView
            lvList.setVisibility(View.VISIBLE);
            gvList.setVisibility(View.GONE);
            btnDisplay.setImageResource(R.drawable.icon_pic_grid_type);
            isListView = true;
        }
    }

    class PhotosAdapter extends BaseAdapter{

        /**
         * BitmapUtils的对象实例
         */
        private BitmapUtils mBitmapUtils;

        public PhotosAdapter() {
            mBitmapUtils = new BitmapUtils(mActivity);
            mBitmapUtils.configDefaultLoadingImage(R.drawable.pic_item_list_default);
        }

        @Override
        public int getCount() {
            return mPhotoList.size();
        }

        @Override
        public PhotosBean.PhotoNews getItem(int position) {
            return mPhotoList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null){
                convertView = View.inflate(mActivity,R.layout.list_item_photo,null);
                holder = new ViewHolder();
                holder.tvTitle = (TextView) convertView.findViewById(R.id.tv_title4);
                holder.ivPic = (ImageView) convertView.findViewById(R.id.iv_pic);
                convertView.setTag(holder);
            }else {
                holder = (ViewHolder) convertView.getTag();
            }
            PhotosBean.PhotoNews item = getItem(position);
            holder.tvTitle.setText(item.title);
            mBitmapUtils.display(holder.ivPic,item.listimage);
            return convertView;
        }
    }

    static class ViewHolder{
        public TextView tvTitle;
        public ImageView ivPic;
    }
}
