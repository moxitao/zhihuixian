package com.example.zhbj.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * 基类Fragment
 */
public abstract class BaseFragment extends Fragment {

    /**
     * Activity对象，可以当作Context去使用
     */
    public Activity mActivity;

    /**
     * View对象，Fragment的根布局
     */
    public View mRootView;

    // Fragment创建
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = getActivity(); // 获取Fragment所依赖的Activity对象
    }

    // Fragment初始化布局
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = initView();
        return mRootView;
    }

    // Fragment所在的activity创建完成
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initData();
    }

    /**
     * 初始化布局，抽象方法，需要由子类来实现
     */
    public abstract View initView();

    /**
     * 初始化数据，子类可以不实现
     */
    public void initData(){

    }
}
