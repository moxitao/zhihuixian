package com.example.zhbj.fragment;

import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

import com.example.zhbj.MainActivity;
import com.example.zhbj.R;
import com.example.zhbj.base.BasePaper;
import com.example.zhbj.base.impl.GovAffairsPaper;
import com.example.zhbj.base.impl.HomePaper;
import com.example.zhbj.base.impl.NewsCenterPaper;
import com.example.zhbj.base.impl.SettingPaper;
import com.example.zhbj.base.impl.SmartServicePaper;
import com.example.zhbj.view.NoScrollViewPaper;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

import java.util.ArrayList;

/**
 * 主页面Fragment
 */
public class ContentFragment extends BaseFragment {

    /**
     * 获取到ViewPaper的对象
     */
    @ViewInject(R.id.vp_content)
    private NoScrollViewPaper mViewPaper;

    /**
     * 存储5个标签页面的集合
     */
    private ArrayList<BasePaper> mList;

    /**
     * Gruop的对象
     */
    @ViewInject(R.id.rg_group)
    private RadioGroup rgGroup;

    @Override
    public View initView() {
        View view = View.inflate(mActivity, R.layout.fragment_content, null);
        ViewUtils.inject(this,view);
        //mViewPaper = (NoScrollViewPaper) view.findViewById(R.id.vp_content);
        //rgGroup = (RadioGroup) view.findViewById(R.id.rg_group);
        return view;
    }

    /**
     * 初始化ViewPaper的数据
     */
    @Override
    public void initData() {
        // 初始化5个标签页面对象
        mList = new ArrayList<>();
        mList.add(new HomePaper(mActivity));
        mList.add(new NewsCenterPaper(mActivity));
        mList.add(new SmartServicePaper(mActivity));
        mList.add(new GovAffairsPaper(mActivity));
        mList.add(new SettingPaper(mActivity));
        mViewPaper.setAdapter(new ContentAdapter());

        rgGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.rb_home:
                        // 首页
                        mViewPaper.setCurrentItem(0,false);
                        break;
                    case R.id.rb_news:
                        // 新闻中心
                        mViewPaper.setCurrentItem(1,false);
                        break;
                    case R.id.rb_smart:
                        // 智慧服务
                        mViewPaper.setCurrentItem(2,false);
                        break;
                    case R.id.rb_gov:
                        // 政务
                        mViewPaper.setCurrentItem(3,false);
                        break;
                    case R.id.rb_setting:
                        // 设置
                        mViewPaper.setCurrentItem(4,false);
                        break;
                    default:break;
                }
            }
        });

        // 监听Viewpager页面切换时间，初始化当前页面数据
        mViewPaper.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                BasePaper paper = mList.get(i);
                paper.initData();

                if (i == 0 || i == mList.size() - 1){
                    // 禁用侧边栏，首页和设置页
                    setSlidingMenuEnable(false);
                }else {
                    // 启用侧边栏
                    setSlidingMenuEnable(true);
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
        // 手动初始化第一个页面的数据
        mList.get(0).initData();

        // 手动禁用第一个页面的侧边栏
        setSlidingMenuEnable(false);
    }

    /**
     * 开启/禁用侧边栏
     */
    private void setSlidingMenuEnable(boolean enable){
        // 获取MainActivity对象
        MainActivity mainUI = (MainActivity) mActivity;

        // 获取SlidingMenu对象
        SlidingMenu slidingMenu = mainUI.getSlidingMenu();
        if (enable){
            slidingMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
        }else {
            slidingMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);
        }

    }

    /**
     * ViewPaper的实现类
     */
    class ContentAdapter extends PagerAdapter{

        @Override
        public int getCount() {
            return mList.size();
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
            return view == o;
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            // 获取当前页面的对象
            BasePaper paper = mList.get(position);

            // 此方法导致每次都提前加载下一页数据，浪费流量和性能，不建议在此处初始化数据
            // paper.initData(); // 初始化布局（给帧布局添加布局对象），以子类实现为准

            // 布局对象
            container.addView(paper.mRootView);

            return paper.mRootView;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            container.removeView((View) object);
        }
    }

    /**
     * 获取新闻中心对象
     */
    public NewsCenterPaper getNewsCenterPager(){
        NewsCenterPaper paper = (NewsCenterPaper) mList.get(1); // 新闻中心在集合的第二个位置
        return paper;
    }
}
