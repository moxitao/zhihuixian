package com.example.zhbj.fragment;

import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.zhbj.MainActivity;
import com.example.zhbj.R;
import com.example.zhbj.base.impl.NewsCenterPaper;
import com.example.zhbj.domain.NewsMenu;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

import java.util.ArrayList;

/**
 * 侧边栏Fragment
 */
public class LeftMenuFragment extends BaseFragment {

    @ViewInject(R.id.lv_menu)
    private ListView lvList;

    /**
     * 侧边栏的数据对象
     */
    private ArrayList<NewsMenu.NewsMenuData> data;

    /**
     * 当前选中的菜单位置
     */
    private int mCurrentPos;

    /**
     * 侧边栏的适配器对象
     */
    private LeftMenuAdapter mAdapter;

    @Override
    public View initView() {
        View view = View.inflate(mActivity, R.layout.fragment_left_menu, null);
        ViewUtils.inject(this,view);
        return view;
    }

    /**
     * 设置侧边栏数据，通过此方法，可以将新闻中心页面将网格数据传递过来
     * @param data
     */
    public void setMenuData(ArrayList<NewsMenu.NewsMenuData> data) {
        this.data = data;

        // 重置当前选中位置归0，避免侧边栏选中位置和菜单详情页不同步
        mCurrentPos = 0;

        mAdapter = new LeftMenuAdapter();
        lvList.setAdapter(mAdapter);
        lvList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mCurrentPos = position; // 更新当前的点击位置
                // 刷新ListView
                mAdapter.notifyDataSetChanged();

                // 收回侧边栏
                toggle();
                
                // 修改侧边栏的帧布局
                setMenuDetailPager(position);
            }
        });
    }

    /**
     * 修改菜单详情页
     * @param position
     */
    private void setMenuDetailPager(int position) {
        // 修改侧边栏的帧布局
        // 获取新闻中心的对象
        MainActivity mainUI = (MainActivity) mActivity;
        ContentFragment fragment = mainUI.getContentFragment();
        NewsCenterPaper pager = fragment.getNewsCenterPager();
        pager.setMenuDetailPager(position);
    }

    /**
     * 控制侧边栏的开关
     */
    private void toggle() {
        MainActivity mainUI = (MainActivity) mActivity;
        SlidingMenu slidingMenu = mainUI.getSlidingMenu();
        slidingMenu.toggle(); // 如果当前为开，则关；反之亦然
    }

    /**
     * 侧边栏的适配器类
     */
    class LeftMenuAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public NewsMenu.NewsMenuData getItem(int position) {
            return data.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = View.inflate(mActivity, R.layout.list_item_left_menu, null);
            TextView tvMenu = (TextView) view.findViewById(R.id.tv_menu);

            // 设置TextView的可用或者不可用的颜色
            if (mCurrentPos == position){
                // 当前item被选中
                tvMenu.setEnabled(true);
            }else {
                // 当前item未选中
                tvMenu.setEnabled(false);
            }

            NewsMenu.NewsMenuData info = getItem(position);
            tvMenu.setText(info.title);
            return view;
        }
    }
}
