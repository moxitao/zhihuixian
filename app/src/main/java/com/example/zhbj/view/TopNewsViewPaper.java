package com.example.zhbj.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * 头条新闻的ViewPager
 */
public class TopNewsViewPaper extends ViewPager {

    /**
     * 起始的X坐标
     */
    private int startX;

    /**
     * 起始的Y坐标
     */
    private int startY;

    /**
     * 终点的X坐标
     */
    private int endX;

    /**
     * 终点的Y坐标
     */
    private int endY;

    /**
     * X坐标的偏移量
     */
    private int dX;

    /**
     * Y坐标的便宜量
     */
    private int dY;

    public TopNewsViewPaper(@NonNull Context context) {
        super(context);
    }

    public TopNewsViewPaper(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * 需要分情况来判断是否需要拦截事件
     * 1.上下滑动，需要拦截
     * 2.左滑时，最后一个页面需要拦截
     * 3.右滑时，第一个页面需要拦截
     * @param ev
     * @return
     */
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        // 请求父控件不拦截事件
        getParent().requestDisallowInterceptTouchEvent(true);

        switch (ev.getAction()){
            case MotionEvent.ACTION_DOWN:
                // 按下
                startX = (int) ev.getX();
                startY = (int) ev.getY();
                break;
            case MotionEvent.ACTION_MOVE:
                // 移动
                endX = (int) ev.getX();
                endY = (int) ev.getY();
                dX = endX - startX;
                dY = endY - startY;
                if (Math.abs(dX) > Math.abs(dY)){
                    // 左右滑动
                    int currentPos = getCurrentItem();
                    if (dX > 0){
                        // 右滑动
                        if (currentPos == 0){
                            getParent().requestDisallowInterceptTouchEvent(false);
                        }
                    }else {
                        // 左滑动
                        int count = getAdapter().getCount(); // 当前viewpager中的item总数
                        if (currentPos == count - 1){
                            getParent().requestDisallowInterceptTouchEvent(false);
                        }
                    }
                }else {
                    // 上下滑动
                    getParent().requestDisallowInterceptTouchEvent(false);
                }
                break;
            default:break;
        }
        return super.dispatchTouchEvent(ev);
    }
}
