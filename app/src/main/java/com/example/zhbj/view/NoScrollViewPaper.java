package com.example.zhbj.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * 禁止滑动的ViewPaper
 */
public class NoScrollViewPaper extends ViewPager {

    public NoScrollViewPaper(@NonNull Context context) {
        super(context);
    }

    public NoScrollViewPaper(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        // 重写父类的onToucheEvent方法，返回true表示此处什么都不做，达到禁用滑动效果的目的
        return true;
    }

    /**
     * 事件中断，拦截
     * @param ev
     * @return
     */
    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        // true:拦截，false:不拦截，传给子控件
        return false; // 标签页的ViewPager不拦截标签菜单详情页页签的ViewPager
    }
}
