# 新闻资讯类App 智慧西安

#### 介绍
一个新闻资讯类App的基本应用，包括了常见的架构模式以及框架的使用

#### 相关技术栈
- 快速敏捷开发
- Xutils（工具包）
- SlidingMenu（恻拉菜单）
- Jpush（极光推送）
- sharesdk（社会分享）
- pullTorefush（下拉刷新）
- Zxing（二维码生成）
- U-App（友盟统计）
- Gson（数据解析）
- Tomcat（服务器）


#### 核心技术点

- Animation（闪屏动画）
- ViewPager（页面切换）
- RadioGroup & RadioButton（灰点指示器）
- Fragment（页面显示）
- ViewPagerIndicator（顶层滑动栏）
- CirclePageIndicator（圆点指示器）
- WebView（显示数据）
- AlertDialog.Builder（设置页面）

#### 浏览
##### 闪屏动画
![输入图片说明](https://images.gitee.com/uploads/images/2020/0328/161839_cafb74b7_5037130.png "屏幕截图.png")
##### 主页
![输入图片说明](https://images.gitee.com/uploads/images/2020/0328/162017_d994b357_5037130.png "屏幕截图.png")
##### 新闻页面（未开启服务器）
![输入图片说明](https://images.gitee.com/uploads/images/2020/0328/162028_7e65d2fb_5037130.png "屏幕截图.png")
##### 新闻页面（开启服务器）
![输入图片说明](https://images.gitee.com/uploads/images/2020/0328/162119_6877687d_5037130.png "屏幕截图.png")
##### 侧边栏
![输入图片说明](https://images.gitee.com/uploads/images/2020/0328/162158_a4406ddd_5037130.png "屏幕截图.png")
##### 其他内容
![输入图片说明](https://images.gitee.com/uploads/images/2020/0328/162220_ccfc972f_5037130.png "屏幕截图.png")
##### 新闻内容
![输入图片说明](https://images.gitee.com/uploads/images/2020/0328/162302_7626bffb_5037130.png "屏幕截图.png")
##### 字体设置
![输入图片说明](https://images.gitee.com/uploads/images/2020/0328/162314_162f43b0_5037130.png "屏幕截图.png")